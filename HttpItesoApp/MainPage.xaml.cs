﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace HttpItesoApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            GetHeaders();
        }

        public async void GetAString()
        {
            try
            {
                HttpClient httpclient = new HttpClient();
                string content = await httpclient.GetStringAsync(
                    new Uri("http://www.bing.com"));
                txtContent.Text += content;
            }
            catch (Exception ex)
            {
                //...
            }
        }

        public async void GetStatusCodes()
        {
            try
            {
                var client = new HttpClient();
                var uri = new Uri("http://www.bing.com");
                var response = await client.GetAsync(uri);
                var statuscode = response.StatusCode;
                //response.EnsureSuccessStatusCode();
                txtResponses.Text += " " + statuscode.ToString();
            }
            catch (Exception ex)
            {

            }
        }

        public async void GetHeaders()
        {
            try
            {
                var client = new HttpClient();
                var uri = new Uri("http://www.bing.com");
                var Response = await client.GetAsync(uri);

                Response.EnsureSuccessStatusCode();

                foreach(var header in Response.Headers)
                {
                    string headerItem = header.Key + " = " + header.Value;
                    txtHeaders.Text += headerItem;

                }
            }
            catch (Exception ex)
            { }
        }
    }
}
